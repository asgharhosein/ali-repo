# README #

This package contains the V-REP simulator for the DimRob project. It includes the VREP scene which contains the robot model, as well as code to establish the interface between V-REP and ROS.

Version: Initial release on 9/10/14

To use this package:
-Install V-REP and make sure it is communicating with ROS (http://www.coppeliarobotics.com/helpFiles/en/rosTutorialHydro.htm)

-Clone this directory to the 'src' directory of your catkin workspace and build.

-You need to create a symbolic link to the generated 'dimRobSimInterface' executable (catkin_ws/devel/lib/dimrob_sim/dimRobSimulation) in the vrep home folder, or else V-REP will not find it.

-Open V-REP, load the scene included in the top level of this directory ('dimRos_Simulation.ttt'), and run

-You can run 'rosrun dimrob_sim sim_test' to test communication between V-REP and ROS

Contact Tim Sandy at tsandyATethzDOTch with questions